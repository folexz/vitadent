//
//  PricesTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 05/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import UIKit
import Crashlytics

class PricesTableViewController: UITableViewController {
    
    // MARK: Variables
	var currentSection: Int = 0 {
        didSet {
            if (currentSection != oldValue) {
                switch currentSection {
                case 1: self.prices = PricesManager.pricesWith(type: .terapyServices)
                case 2: self.prices = PricesManager.pricesWith(type: .ortopedyServices)
                case 3: self.prices = PricesManager.pricesWith(type: .surgeryServices)
                case 4: self.prices = PricesManager.pricesWith(type: .childrenServices)
                case 5: self.prices = PricesManager.pricesWith(type: .warning)
                default: break
                }
            }
        }
    }
	
	var prices = [Price]()
   
    // MARK: - UIViewController
    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200

    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 24
        case 1: return 23
        case 2: return 18
        case 3: return 18
        case 4: return 1
        default: return 0
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Терапия"
        case 1: return "Ортопедия"
        case 2: return "Хирургия"
        case 3: return "Цены детского отделения"
        case 4: return "Внимание!"
        default: return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        self.currentSection = indexPath.section + 1
        
        if indexPath.section == 4 {

            let cell = UITableViewCell(style: .default,
                                       reuseIdentifier: "warning")
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont(name: "SFUIText-Light",
                                          size: 16)
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = self.prices[0].name
            return cell

        } else {

            let cell = tableView.dequeueReusableCell(withIdentifier: "price",
                                                     for: indexPath) as! PricesTableViewCell

            cell.name?.font = UIFont(name: "SFUIText-Light",
                                     size: 16)
            cell.price?.font = UIFont(name: "SFUIText-Bold",
                                      size: 16)
            cell.name?.numberOfLines = 0
            
            cell.name?.text = prices[indexPath.row].name
            cell.price?.text = "\(prices[indexPath.row].price) руб."
            
            return cell

        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.tableView.deselectRow(at: indexPath,
                                   animated: true)
        
        if indexPath.section != 4 {
            let serviceName = (tableView.cellForRow(at: indexPath) as! PricesTableViewCell).name.text
            UIAlertController.callBackMenuWith(name: serviceName!,
                                               controller: self)
        }
        
    }
    
}
