//
//  CoreDataStack.swift
//  poemOfDay
//
//  Created by Felix Zubarev on 16.07.16.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {

    static let instance = CoreDataManager()

    // MARK: - Core Data stack
    func entityFor(name: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: name, in: self.persistentContainer.viewContext)!
    }

    func fetchedResultsControllerWith(key: String) -> NSFetchedResultsController<Sales> {
        let request: NSFetchRequest<Sales> = Sales.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: key,
                                              ascending: true)
        request.sortDescriptors = [sortDescriptor]
        let controller = NSFetchedResultsController(fetchRequest: request,
                                                    managedObjectContext: CoreDataManager.instance.persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        return controller
    }

    // MARK: - Core Data saving support
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "vitaDent")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
