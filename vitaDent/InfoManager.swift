//
//  InfoManager.swift
//  vitaDent
//
//  Created by Felix Zubarev on 11/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import Foundation
import SwiftyJSON

enum WebType: String {
    case service = "Services"
    case specialist = "Specialists"
    case children = "ChildrenServices"
}

struct Info {
    var name: String
    var type: String?
    var url: String
}

class InfoManager {
	
	class func infoWith(type: WebType) -> [Info] {
        
        var count: Int {
			switch type {
			case .service: return 8
			case .specialist: return 10
			case .children: return 7
			}
        }
        
        var json: JSON {
			guard let path = Bundle.main.path(forResource: type.rawValue, ofType: "json") else {return JSON.null}
			do {
				let data = try Data(contentsOf: URL(fileURLWithPath: path))
				return JSON(data: data)
			} catch {
				return JSON.null
			}
		}
        
        var info = [Info]()
        
        for i in 1...count {
            let obj = json["\(i)"]
            let newInfo = Info(name: obj["name"].string!,
                               type: obj["type"].string,
                               url:  obj["url"].string!)
            info.append(newInfo)
        }
        
        return info
        
    }
	
}
