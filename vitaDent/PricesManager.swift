//
//  PricesManager.swift
//  vitaDent
//
//  Created by Felix Zubarev on 25/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Price {
    var name: String
    var price: Int
}

enum PriceType: String {
    case terapyServices
    case ortopedyServices
    case surgeryServices
    case childrenServices
    case warning
}

class PricesManager {
	
    class func pricesWith(type: PriceType) -> [Price] {
		
		var count: Int {
			switch type {
			case .terapyServices: return 24
			case .ortopedyServices: return 23
			case .surgeryServices: return 18
			case .childrenServices: return 18
			case .warning: return 1
			}
		}
		
        var json: JSON {
			guard let path = Bundle.main.path(forResource: "Prices", ofType: "json") else {return JSON.null}
			do {
				let data = try Data(contentsOf: URL(fileURLWithPath: path))
				return JSON(data: data)[type.rawValue]
			} catch {
				return JSON.null
			}
		}
        
        var prices = [Price]()
        
		for i in 1...count {
            let obj = json["\(i)"]
            let newPrice = Price(name: obj["name"].string!,
                                 price: obj["price"].int!)
            prices.append(newPrice)
        }
		
        return prices
        
    }
	
}
