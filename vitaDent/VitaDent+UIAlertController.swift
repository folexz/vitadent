//
//  VitaDent+UIAlertController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 07/02/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    public static func callBackMenuWith(name: String, controller: UIViewController) {
        
        let alert = UIAlertController(title: "\(name)",
            message:"Вы можете позвонить нам сами, либо заказать обратный звонок и получить скидку 5% при оформлении услуги:",
            preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction.callActionWith(phoneNumber: ["+7 (3812) 36-35-35",
                                                                   "tel://73812363535"]))
        alert.addAction(UIAlertAction.callBackActionWith(name: name,
                                                         controller: controller))
        alert.addAction(UIAlertAction.cancelAction())
        
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    public static func callBackAlertWith(name: String, phone: String, controller: UIViewController) {
        
        let alert = UIAlertController(title: "Заказать обратный звонок",
                                      message: "Прежде вы сохраняли номер\n\(phone).\nЭто ваш текущий номер?",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.yesInAlertActionWith(phone: phone,
                                                           name: name))
		
        alert.addAction(UIAlertAction.noInAlertActionWith(name: name,
                                                          controller: controller))
        
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    public static func contactsMenuFrom(controller: UIViewController) {
        
        let alert = UIAlertController(title: "Выберите действие",
                                      message: "Вы можете позвонить нам по телефону, либо связаться с нами в Viber",
                                      preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction.callActionWith(phoneNumber: ["Позвонить по телефону",
                                                                       "tel://79039279500"]))
        alert.addAction(UIAlertAction.callActionWith(phoneNumber: ["Связаться в Viber",
                                                                   "viber://add?number=79039279500"]))
        alert.addAction(UIAlertAction.cancelAction())
        
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    public static func memberPhoneAlertWith(phone: String, controller: UIViewController) {
		
        let alert = UIAlertController(title: "Запомнить номер",
                                      message: "Сохранить номер\n\(phone)\nдля последующих обращений по обратному звонку? Ваш номер не передается третьим лицам и хранится у Вас на телефоне",
									  preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.yesInMemberActionWith(phone: phone))
        alert.addAction(UIAlertAction.noInMemberActionFrom(controller: controller))
        
        controller.present(alert, animated: true, completion: nil)

    }
    
    public static func wrongPhoneAlertFrom(controller: UIViewController) {
        
        let alert = UIAlertController(title: "Неверно введен номер",
                                      message: "Пожалуйста, введите правильный номер",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.okAction())
        
        controller.present(alert, animated: true, completion: nil)

    }

}
