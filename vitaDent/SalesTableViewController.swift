//
//  SalesTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 05/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

class SalesTableViewController: UITableViewController {

    // MARK: - Variables
    
    ///Fetches all available Sales from CoreData
    var sales: [Sales] {
        ///Fetched results sorted by id
        let resultsVC = CoreDataManager.instance.fetchedResultsControllerWith(key: "id")
        do {
            //Delete all disabled Sales from array
            try resultsVC.performFetch()
            return resultsVC.fetchedObjects!
        } catch {
            print(error)
            return []
        }

    }

    // MARK: - UIViewController
    
    override func viewDidLoad() {

        super.viewDidLoad()

        self.refreshControl?.addTarget(self,
                                       action: #selector(refreshTable),
                                       for: .allEvents)
        self.view.addSubview(self.refreshControl!)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.callBackWith(target: self,
                                                                              action: #selector(callBack))
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sales.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "sale",
                                                 for: indexPath) as! SalesTableViewCell

        cell.name.text = self.sales[indexPath.row].name
        cell.name.font = UIFont(name: "SFUIText-Medium", size: 18)
        cell.name.numberOfLines = 0
        cell.desc.text = self.sales[indexPath.row].text
        cell.desc.font = UIFont(name: "SFUIText-Regular", size: 16)
        cell.desc.numberOfLines = 0
        cell.img.image = UIImage(named: "\(self.sales[indexPath.row].id)")
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath,
                                   animated: true)
        UIAlertController.callBackMenuWith(name: (tableView.cellForRow(at: indexPath) as! SalesTableViewCell).name.text!,
                                           controller: self)
        
    }
    
    func callBack() {
        UIAlertController.callBackMenuWith(name: "Акции",
                                           controller: self)
    }

    ///Refreshes table
    func refreshTable() {
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }

}
