//
//  BackCallTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 17/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit
import PhoneNumberKit

///Class that hadles Back Call action
class BackCallTableViewController: UITableViewController {

    // MARK: Outlets
    
    ///Field of the phone number
    @IBOutlet weak var textField: PhoneNumberTextField!
    
    // MARK: - Variables
	var serviceName: String!
    var alreadyUsed = false
    var alreadyUsedState: String {
		return alreadyUsed ? "Уже использовал услугу обратного звонка прежде" : "Еще не пользовался услугой обратного звонка"
    }
    
    // MARK: - Selector functions of UIBarButtonItems
    func dismissController() {
        self.dismiss(animated: true,
                     completion: nil)
    }
    
    func sendPhoneToFirebase() {
        
        let info: [String : Any?] = ["date" :			DateManager.instance.currentDate[0],
                                     "localizedDate" :	DateManager.instance.currentDate[1],
                                     "phone" :			self.textField.text,
                                     "service" :		serviceName,
                                     "alreadyCalled" :	alreadyUsedState]
        
        if self.textField.isValidNumber {
            FirebaseManager.sendPhoneToFirebaseWith(info: info,
                                                             date: DateManager.instance.currentDate[0])
            UIAlertController.memberPhoneAlertWith(phone: self.textField.text!,
                                                   controller: self)
        } else {
            UIAlertController.wrongPhoneAlertFrom(controller: self)
        }
        
    }
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.textField.defaultRegion = "RU"
        self.textField.font = UIFont(name: "SFUIText-Light",
                                     size: 15)
        self.textField?.becomeFirstResponder()
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.cancelButtonWith(target: self,
                                                                                 action: #selector(dismissController))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.doneButtonWith(target: self,
                                                                                action: #selector(sendPhoneToFirebase))

    }

}
