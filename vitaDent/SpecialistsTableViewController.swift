//
//  SpecialistsTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 07/02/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

class SpecialistsTableViewController: UITableViewController {

	// MARK: Variables
	var specialists: [Info] {
		return InfoManager.infoWith(type: .specialist)
	}

	//MARK: - UIViewController
	override func viewDidLoad() {
		
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.callBackWith(target: self,
                                                                              action: #selector(callBack))
        
        self.tableView.rowHeight = 80
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func callBack() {
        UIAlertController.callBackMenuWith(name: "Из специалистов",
                                           controller: self)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 4
        case 1: return 2
        case 2: return 2
        case 3: return 1
        case 4: return 1
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Терапевты"
        case 1: return "Ортопеды"
        case 2: return "Хирурги"
        case 3: return "Ортодонты"
        case 4: return "Зубные техники"
        default: return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var currentRow: Int {
            var row = 0
            switch indexPath.section {
            case 1: row += 4
            case 2: row += 6
            case 3: row += 8
            case 4: row += 9
            default: row += 0
            }
            row += indexPath.row
            return row
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "specialist",
                                                 for: indexPath) as! ImageTextTableViewCell
        
        cell.serviceImage.image = UIImage(named: self.specialists[currentRow].url)
        cell.name.font = UIFont(name: "SFUIText-Medium",
                                size: 18)
        cell.name.text = specialists[currentRow].name
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var chosenRow: Int {
            var row = 0
            switch indexPath.section {
            case 1: row += 4
            case 2: row += 6
            case 3: row += 8
            case 4: row += 9
            default: row += 0
            }
            row += indexPath.row
            return row
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewController
        vc.info = self.specialists[chosenRow]
        
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
        
    }

}
