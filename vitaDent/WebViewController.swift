//
//  WebViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 11/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var webView: UIWebView!

    // MARK: - Variables
    var info: Info!

	//MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.callBackWith(target: self,
                                                                              action: #selector(callBack))
		
        self.automaticallyAdjustsScrollViewInsets = false
        
        let url = Bundle.main.url(forResource: info.url,
                                  withExtension: "html")
        
        self.webView.loadRequest(URLRequest(url: url!))
        
        if let name = self.info.type {
            self.navigationItem.title = name
        } else {
            self.navigationItem.title = self.info.name
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                                style: .plain,
                                                                target: nil,
                                                                action: nil)
        
    }
    
    func callBack() {
        UIAlertController.callBackMenuWith(name: self.info.name,
                                           controller: self)
    }
    
}
