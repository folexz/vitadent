//
//  DefaultButtons.swift
//  vitaDent
//
//  Created by Felix Zubarev on 04/02/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    public static func callBackWith(target: Any?, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(title: "Обр. звонок",
                               style: .plain,
                               target: target,
                               action: action)
    }
    
    public static func cancelButtonWith(target: Any?, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(title: "Отмена",
                               style: .plain,
                               target: target,
                               action: action)
    }
    
    public static func doneButtonWith(target: Any?, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(title: "Отправить",
                               style: .done,
                               target: target,
                               action: action)
    }

    
}
