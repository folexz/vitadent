//
//  DateManager.swift
//  vitaDent
//
//  Created by Felix Zubarev on 17/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit
import DateTools

class DateManager {
    
    static let instance = DateManager()
    
    private func localized(month: Int) -> String {
        switch month {
        case 1: return "Января"
        case 2: return "Февраля"
        case 3: return "Марта"
        case 4: return "Апреля"
        case 5: return "Мая"
        case 6: return "Июня"
        case 7: return "Июля"
        case 8: return "Августа"
        case 9: return "Сентября"
        case 10: return "Октября"
        case 11: return "Ноября"
        case 12: return "Декабря"
        default: return ""
        }
    }
    
    var currentDate: [String] {
        
        let date = NSDate()
        
        var month: String {
            if date.month() < 10 {
                return "0\(date.month())"
            } else {
                return "\(date.month())"
            }
        }
        
        var day: String {
            if date.day() < 10 {
                return "0\(date.day())"
            } else {
                return "\(date.day())"
            }
        }
        
        var hour: String {
            if date.hour() < 10 {
                return "0\(date.hour())"
            } else {
                return "\(date.hour())"
            }
        }
        
        var minute: String {
            if date.minute() < 10 {
                return "0\(date.minute())"
            } else {
                return "\(date.minute())"
            }
        }
        
        var second: String {
            if date.second() < 10 {
                return "0\(date.second())"
            } else {
                return "\(date.second())"
            }
        }
        
        return ["\(NSDate().year())-\(month)-\(day) \(hour):\(minute):\(second)",
			"\(date.day()) \(self.localized(month: date.month())) \(date.year()) года в \(hour):\(minute):\(second)"]
          
    }
    
}
