//
//  VitaDent+UIAlertAction.swift
//  vitaDent
//
//  Created by Felix Zubarev on 07/02/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

extension UIAlertAction {
    
    public static func callActionWith(phoneNumber: [String]) -> UIAlertAction {
		return UIAlertAction(title: phoneNumber[0],
                      style: .default,
                      handler: {(_) -> Void in
                                    UIApplication.shared.open(URL(string: phoneNumber[1])!,
                                                                  options: [:],
                                                                  completionHandler: nil)
                                })
    }
    
    public static func callBackActionWith(name: String, controller: UIViewController) -> UIAlertAction {
        
        return UIAlertAction(title: "Заказать обратный звонок",
                             style: .default,
                             handler: {(_) -> Void in
                                            if let phone = UserDefaultsManager.phone {
                                                UIAlertController.callBackAlertWith(name: name,
																					phone: phone,
																					controller: controller)
                                            } else {
												self.presentBackCallWith(name: name,
												                         alreadyUsed: false,
												                         controller: controller)
                                            }
                                
                                      })
    }
    
    public static func yesInAlertActionWith(phone: String, name: String) -> UIAlertAction {
        return UIAlertAction(title: "Да",
                                      style: .default,
                                      handler: {(_) -> Void in
                                        
                                        let info: [String : Any?] = ["date" : DateManager.instance.currentDate[0],
                                                            "localizedDate" : DateManager.instance.currentDate[1],
                                                                             "phone" : phone,
                                                                             "service" : name,
                                                                             "alreadyCalled" : "Уже использовал услугу обратного звонка прежде"]
                                        
                                                FirebaseManager.sendPhoneToFirebaseWith(info: info,
                                                                                                 date: DateManager.instance.currentDate[0])
                                        
                                                })
    }
    
    public static func noInAlertActionWith(name: String, controller: UIViewController) -> UIAlertAction {
        return UIAlertAction(title: "Нет",
                                     style: .cancel,
                                     handler: {(_) -> Void in
										self.presentBackCallWith(name: name,
										                         alreadyUsed: true,
										                         controller: controller)
                                              })
    }
    
    public static func yesInMemberActionWith(phone: String) -> UIAlertAction {
        return UIAlertAction(title: "Да, сохранить",
                      style: .default,
                      handler: {(alert) -> Void in
                                UserDefaultsManager.phone = phone
                                var rootViewController = UIApplication.shared.keyWindow?.rootViewController
                                if let navigationController = rootViewController as? UINavigationController {
                                    rootViewController = navigationController.viewControllers.first
                                }
                                if let tabBarController = rootViewController as? UITabBarController {
                                    rootViewController = tabBarController.selectedViewController
                                }
                                rootViewController?.dismiss(animated: true, completion: nil)
                                })
    }
    
    public static func noInMemberActionFrom(controller: UIViewController) -> UIAlertAction {
        return UIAlertAction(title: "Нет, не надо",
                             style: .cancel,
                             handler: {(_) -> Void in
                                controller.dismiss(animated: true, completion: nil)
                             })
    }
    
    public static func okAction() -> UIAlertAction {
        return UIAlertAction(title: "Ок",
                             style: .cancel,
                             handler: nil)
    }
    
    public static func cancelAction() -> UIAlertAction {
        return UIAlertAction(title: "Отмена",
                             style: .destructive,
                             handler: nil)
    }
    
    private static func presentBackCallWith(name: String, alreadyUsed: Bool, controller: UIViewController) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "backCall")
        
        let backCallVC = vc.childViewControllers[0] as! BackCallTableViewController
        
        backCallVC.serviceName = name
        backCallVC.alreadyUsed = alreadyUsed
        
        controller.present(vc, animated: true, completion: nil)
        
    }
    
}
