//
//  Annotation.swift
//  vitaDent
//
//  Created by Felix Zubarev on 11/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import Contacts

class Annotation: NSObject, MKAnnotation {

	var title: String?
	var subtitle: String?
    var coordinate: CLLocationCoordinate2D

	var mapItem: MKMapItem {
		let addressDictionary = [String(CNPostalAddress().street): self.subtitle]
		let placemark = MKPlacemark(coordinate: coordinate,
		                            addressDictionary: addressDictionary)
		
		let mapItem = MKMapItem(placemark: placemark)
		mapItem.name = self.title
		
		return mapItem
	}
	
	init(title: String, coordinate: CLLocationCoordinate2D, andSubtitle subtitle: String) {
		self.title = title
		self.coordinate = coordinate
		self.subtitle = subtitle
	}
	
}
