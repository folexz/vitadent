//
//  VitaDent+URL.swift
//  vitaDent
//
//  Created by Felix Zubarev on 07/02/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit
import SafariServices

extension URL {
    
	public static func openSFSVCWith(url: String, controller: UIViewController) {
        
		let vc = SFSafariViewController(url: URL(string: url)!)
        controller.present(vc,
                           animated: true,
                           completion: nil)
        
    }
	
    public static func open(url: String) {
        UIApplication.shared.open(URL(string: url)!,
                                  options: [:],
                                  completionHandler: nil)
    }
    
}
