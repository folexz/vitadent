//
//  ImageTextTableViewCell.swift
//  vitaDent
//
//  Created by Felix Zubarev on 05/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import UIKit

class ImageTextTableViewCell: UITableViewCell {
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var name: UILabel!
}

class ChildrenTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
}

class PricesTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
}

class SalesTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var img: UIImageView!
}

class CustomTabBarController: UITabBarController {
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		self.view.superview?.layer.cornerRadius = 25
		
	}
	
}
