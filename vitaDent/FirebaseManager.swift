//
//  FirebaseManager.swift
//  vitaDent
//
//  Created by Felix Zubarev on 05/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import Firebase
import CoreData

class FirebaseManager {

    class func startWatchingForSales() {

        FIRDatabase.database().reference().child("sales_release").observe(.childAdded, with: {(snapshot) -> Void in
            let newSale = snapshot.value as! [String : AnyObject]
            add(newSale)
        })

        FIRDatabase.database().reference().child("sales_release").observe(.childChanged, with: {(snapshot) -> Void in
            let saleToEdit = snapshot.value as! [String : AnyObject]
            edit(saleToEdit)
        })

        FIRDatabase.database().reference().child("sales_release").observe(.childRemoved, with: {(snapshot) -> Void in
            let saleToDelete = snapshot.value as! [String : AnyObject]
            delete(saleToDelete)
        })

    }

	//TODO: Fix It!
    class func downloadImagesForSales() {
        
        let reference = FIRStorage.storage().reference(forURL: "gs://vitadent-7faa9.appspot.com/")
        
        for i in 1...3 {
            
            let islandRef = reference.child("\(i).png")
            
            var imagePath: URL {
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                var imagePath = paths[0]
                imagePath.appendPathComponent("\(i).png")
                return imagePath
            }
            
            islandRef.write(toFile: imagePath) {(url, error) -> Void in
                if let error = error {
                    print(error)
                } else {
                    let request: NSFetchRequest<Sales> = Sales.fetchRequest()
                    let predicate = NSPredicate(format: "id == %d", Int16(i))
                    request.predicate = predicate
                    
                    var saleToAddImage: Sales? {
                        var obj: [Sales]? {
                            do {
                                return try CoreDataManager.instance.persistentContainer.viewContext.fetch(request)
                            } catch {
                                print(error)
                                return nil
                            }
                        }
                        if let sale = obj?[0] {
                            return sale
                        } else {
                            return nil
                        }
                    }
                    
                    guard let _ = saleToAddImage?.image else {
                        saleToAddImage?.image = url?.absoluteString
                        return
                    }
                    
                    CoreDataManager.instance.saveContext()
                    
                }
            }
            
        }
        
    }
    
	private class func add(_ sale: [String : AnyObject]) {

        let request: NSFetchRequest<Sales> = Sales.fetchRequest()
        let predicate = NSPredicate(format: "id == %d", sale["id"] as! Int16)
        request.predicate = predicate

        var obj: Int {
            do {
                return try CoreDataManager.instance.persistentContainer.viewContext.count(for: request)
            } catch {
                return -1
            }
        }

        if obj == 0 {
            let newSale = Sales(context: CoreDataManager.instance.persistentContainer.viewContext)
            newSale.name = sale["name"] as? String
            newSale.text = sale["text"] as? String
            newSale.id = (sale["id"] as? Int16)!
            CoreDataManager.instance.saveContext()
        }

    }

    private class func edit(_ sale: [String : AnyObject]) {

        let request: NSFetchRequest<Sales> = Sales.fetchRequest()
        let predicate = NSPredicate(format: "id == %d", sale["id"]! as! Int16)
        request.predicate = predicate

        var saleToEdit: Sales? {
            var obj: [Sales]? {
                do {
                    return try CoreDataManager.instance.persistentContainer.viewContext.fetch(request)
                } catch {
                    return nil
                }
            }
			return obj?[0] ?? nil
        }

		guard (saleToEdit != nil) else {return}
		
		saleToEdit?.name = sale["name"] as? String
		saleToEdit?.text = sale["text"] as? String
		saleToEdit?.id = (sale["id"] as? Int16)!
		CoreDataManager.instance.saveContext()

    }

    private class func delete(_ sale: [String : AnyObject]) {

        let request: NSFetchRequest<Sales> = Sales.fetchRequest()
        let predicate = NSPredicate(format: "id == %d", sale["id"] as! Int16)
        request.predicate = predicate

        var obj: [Sales]? {
            do {
               return try CoreDataManager.instance.persistentContainer.viewContext.fetch(request)
            } catch {
                return nil
            }
        }

		guard (obj != nil) else {return}
		
		CoreDataManager.instance.persistentContainer.viewContext.delete(obj![0] as NSManagedObject!)
		CoreDataManager.instance.saveContext()

    }
    
    class func sendPhoneToFirebaseWith(info: [String : Any?], date: String) {
        FIRDatabase.database().reference().child("Calls").child("PhonesToCall").child(date).setValue(info)
    }
    
}
