//
//  Sales+CoreDataProperties.swift
//  
//
//  Created by Felix Zubarev on 08/02/2017.
//
//

import Foundation
import CoreData


extension Sales {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sales> {
        return NSFetchRequest<Sales>(entityName: "Sales");
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var text: String?
    @NSManaged public var image: String?

}
