//
//  ContactsTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 11/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import UIKit
import MapKit
import MessageUI
import SafariServices
import Crashlytics
import VTAcknowledgementsViewController

/**
 Defines to whom email will be sent
 - Developer: to Developer
 - VitaDent: to VitaDent
 */
enum EmailType {
    case developer
    case vitaDent
}

class ContactsTableViewController: UITableViewController, MKMapViewDelegate, MFMailComposeViewControllerDelegate {

    // MARK: Outlets
    ///MapView
    @IBOutlet weak var map: MKMapView!

    // MARK: - Variables
    ///Radius of a region
    let regionRadius: CLLocationDistance = 1000

    ///Centers map on location
    func centerMapOn(location: CLLocation) {

        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0,
                                                                  regionRadius * 2.0)
        map.setRegion(coordinateRegion,
                      animated: true)
        
    }

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100

        map.delegate = self

        let mapLocation = CLLocation(latitude: CLLocationDegrees(exactly: 54.9738)!,
                                     longitude: CLLocationDegrees(exactly: 73.4113)!)

        map.setCenter(mapLocation.coordinate,
                      animated: true)
        
        centerMapOn(location: mapLocation)

        let annotation = Annotation(title: "ВитаДент",
                                    coordinate: mapLocation.coordinate,
                                    andSubtitle: "Стоматологическая клиника")

		
		
        map.addAnnotation(annotation)
        map.selectAnnotation(annotation,
                             animated: true)

    }

    // MARK: - MapKit Delegate Methods

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Annotation {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation,
                                           reuseIdentifier: identifier)
                view.pinTintColor = #colorLiteral(red: 0, green: 0.7036370039, blue: 0.6073661447, alpha: 1)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -10, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            return view
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Annotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem.openInMaps(launchOptions: launchOptions)
    }

    // MARK: - TableViewController Data Source

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.tableView.deselectRow(at: indexPath,
                                   animated: true)

        switch indexPath.row {
        case 0 where indexPath.section == 0: self.openMaps()
		case 0 where indexPath.section == 2: URL.openSFSVCWith(url: "http://vitadent-omsk.ru/strahovanie/", controller: self)
        case 0 where indexPath.section == 3: URL.open(url: "tel://73812363535")
        case 1 where indexPath.section == 3: URL.open(url: "tel://73812389500")
        case 2 where indexPath.section == 3: UIAlertController.contactsMenuFrom(controller: self)
        case 0 where indexPath.section == 4: self.sendEmailWith(type: .vitaDent)
        case 1 where indexPath.section == 4: URL.openSFSVCWith(url: "http://vitadent-omsk.ru", controller: self)
        case 0 where indexPath.section == 5: URL.openSFSVCWith(url: "https://vk.com/fellix_z", controller: self)
        case 1 where indexPath.section == 5: self.sendEmailWith(type: .developer)
		case 2 where indexPath.section == 5: self.openAcknowledgements()
        case 3 where indexPath.section == 5: URL.open(url: "https://itunes.apple.com/app/id12345678")
        default: break
        }

    }

    // MARK: - Other Functions
    
    func openMaps() {

        let coordinate = CLLocationCoordinate2DMake(54.9738, 73.4113)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate,
                                                       addressDictionary:nil))
        mapItem.name = "ВитаДент"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])

    }

    func sendEmailWith(type: EmailType) {

        var emailTitle: String {
            switch type {
            case .developer: return "Написать разработчику"
            case .vitaDent: return "Вопрос из приложения"
            }
        }

        var recipient: [String] {
            switch type {
            case .developer: return ["zubfel@gmail.com"]
            case .vitaDent: return ["vitadent@mail.ru"]
            }
        }

        var messageBody: String? {
            switch type {
            case .developer: return "\n\n\n\n\nВитаДент 1.0 (4)\n\(UIDevice.current.systemVersion)"
            case .vitaDent: return nil
            }
        }

        let mc = MFMailComposeViewController()

        UINavigationBar.appearance().barStyle = UIBarStyle.default

        if MFMailComposeViewController.canSendMail() == true {

            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody!,
                              isHTML: false)
            mc.setToRecipients(recipient)
            mc.setNavigationBarHidden(true,
                                      animated: true)
            mc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
            self.present(mc, animated: true,
                         completion: nil)

        }

    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true,
                     completion: nil)
    }
	
	func openAcknowledgements() {
		let vc = VTAcknowledgementsViewController.acknowledgementsViewController()
		
		vc?.navigationItem.title = "Библиотеки"
		self.navigationController?.pushViewController(vc!, animated: true)
		
	}

}
