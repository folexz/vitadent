//
//  ServicesTableViewController.swift
//  vitaDent
//
//  Created by Felix Zubarev on 02/12/2016.
//  Copyright © 2016 Felix Zubarev. All rights reserved.
//

import UIKit

/**
 TableViewController that shows list of all available Services
 */
class ServicesTableViewController: UITableViewController {
    
    //MARK: Variables
	var services: [Info] {
		return InfoManager.infoWith(type: .service)
	}
	var childrenServices: [Info] {
		return InfoManager.infoWith(type: .children)
	}
		
    //MARK: - UIViewController
    override func viewDidLoad() {

        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.callBackWith(target: self, action: #selector(callBack))
        
        self.tableView.estimatedRowHeight = 250
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    }
    
    func callBack() {
        UIAlertController.callBackMenuWith(name: "Из услуг", controller: self)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 80
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 8
        case 1: return 7
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Взрослое отделение"
        case 1: return "Детское отделение"
        default: return ""
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "service",
                                                     for: indexPath) as! ImageTextTableViewCell
            
            cell.serviceImage.image = UIImage(named: "1_\(indexPath.row+1)")
            cell.name.font = UIFont(name: "SFUIText-Medium",
                                    size: 18)
            cell.name.text = self.services[indexPath.row].name
            
            return cell
			
        } else {
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "childrenService",
                                                     for: indexPath) as! ChildrenTableViewCell
            
            cell.name.font = UIFont(name: "SFUIText-Medium",
                                    size: 18)
            cell.name.numberOfLines = 0
            cell.name.text = self.childrenServices[indexPath.row].name
            cell.desc.font = UIFont(name: "SFUIText-Regular",
                                    size: 16)
            cell.desc.numberOfLines = 0
            cell.desc.text = self.childrenServices[indexPath.row].url
            
            return cell
        }

    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.tableView.deselectRow(at: indexPath,
                                   animated: true)
        
        if indexPath.section == 0 {
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewController
            vc.info = self.services[indexPath.row]
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        } else {
			
            UIAlertController.callBackMenuWith(name: "\(self.childrenServices[indexPath.row].name), из детских услуг",
											   controller: self)
			
        }
        
    }

}
