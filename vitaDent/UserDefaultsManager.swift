//
//  UserDefaultsManager.swift
//  vitaDent
//
//  Created by Felix Zubarev on 17/01/2017.
//  Copyright © 2017 Felix Zubarev. All rights reserved.
//

import UIKit

private enum DefaultsKeys: String {
    case phone = "phone"
}

class UserDefaultsManager {
	
    private static var defualts: UserDefaults {
        return UserDefaults(suiteName: "group.com.felix.vitaDent")!
    }
    
	static var phone: String? {
        get {
            return defualts.object(forKey: DefaultsKeys.phone.rawValue) as! String?
        }
        set {
            return defualts.set(newValue, forKey: DefaultsKeys.phone.rawValue)
        }
    }
    
}
